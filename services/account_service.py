import json

from models.clients import Account, Client
from models.sqlalchemy_connection import session_scope, get_instance


class AccountService:

    @staticmethod
    def create_account_by_client(request):
        content_length = int(request.headers['Content-Length'])
        body = request.rfile.read(content_length)
        content = json.loads(body)
        client_id = content.get('client_id', None)
        if not client_id:
            response = json.dumps({'message': 'client_id attribute missing from request body'})
            return response.encode(encoding='utf_8'), 422

        account = Account(client_id=client_id)
        with session_scope() as session:
            client = get_instance(session, Client, id=client_id)
            if not client:
                response = json.dumps({'message': f'Client {client_id} not found'})
                return response.encode(encoding='utf_8'), 404
            session.add(account)
            session.commit()
        response = json.dumps({'message': f'Account {account.id} created successfully for client {client_id}'})
        return response.encode(encoding='utf_8'), 200
