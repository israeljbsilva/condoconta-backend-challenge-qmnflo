import json

from models.clients import Client
from models.sqlalchemy_connection import session_scope


class ClientService:

    @staticmethod
    def create_client(request):
        content_length = int(request.headers['Content-Length'])
        body = request.rfile.read(content_length)
        content = json.loads(body)
        name = content.get('name', None)
        if not name:
            response = json.dumps({'message': 'name attribute missing from request body'})
            return response.encode(encoding='utf_8'), 422

        client = Client(name=name)
        with session_scope() as session:
            session.add(client)
            session.commit()
        response = json.dumps({'message': f'Client {client.id} created successfully'})
        return response.encode(encoding='utf_8'), 200
