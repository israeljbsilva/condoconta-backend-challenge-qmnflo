PIP := pip install -r

PROJECT_NAME := condoconta
PYTHON_VERSION := 3.9.5
VENV_NAME := .venv-$(PROJECT_NAME)-$(PYTHON_VERSION)

# Environment setup
.pip:
	pip install pip --upgrade

setup: .pip
	$(PIP) requirements.txt

.create-venv:
	pyenv install -s $(PYTHON_VERSION)
	pyenv uninstall -f $(VENV_NAME)
	pyenv virtualenv $(PYTHON_VERSION) $(VENV_NAME)
	pyenv local $(VENV_NAME)

create-venv: .create-venv setup

code-convention:
	flake8
	pycodestyle

test:
	pytest --cov-report=term-missing --cov-report=html --cov-report=xml --cov=.

run-postgres:
	docker run --name postgress -p 5434:5434 -e POSTGRES_PASSWORD='$(DATABASE_PASS)' -d postgres:9.2.23-alpine
