from http.server import BaseHTTPRequestHandler

from services.account_service import AccountService
from services.client_service import ClientService


class ClientViews(BaseHTTPRequestHandler):

    base_path = '/api/v1'

    def do_POST(self):
        if self.path == f'{self.base_path}/clients':
            response, status_code = ClientService.create_client(request=self)
            self.send_response(status_code)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(response)

        if self.path == f'{self.base_path}/accounts':
            response, status_code = AccountService.create_account_by_client(request=self)
            self.send_response(status_code)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(response)
