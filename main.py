from http.server import HTTPServer

from config import HOST, PORT

from views.client_views import ClientViews


if __name__ == "__main__":
    httpd = HTTPServer((HOST, PORT), ClientViews)
    print(f'Starting server {HOST} {PORT}')
    httpd.serve_forever()
