import os


HOST = 'localhost'
PORT = 5005

DEBUG = os.getenv('DEBUG', 'False')

DATABASE_NAME = 'postgres'
DATABASE_USER = 'postgres'
DATABASE_PASS = 'passwordpostgres'
DATABASE_URL = os.getenv('DATABASE_URL', f'postgresql://{DATABASE_USER}:{DATABASE_PASS}@localhost:5432/{DATABASE_NAME}')
