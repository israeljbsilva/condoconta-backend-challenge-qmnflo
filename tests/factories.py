from contextlib import contextmanager

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine


ENGINE = create_engine('sqlite:///:memory:')


@contextmanager
def session_scope_factory():
    Session = sessionmaker(bind=ENGINE)
    session = Session()
    session.expire_on_commit = False

    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
