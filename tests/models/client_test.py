from unittest import TestCase

from models.clients import Client, Base
from tests.factories import session_scope_factory, ENGINE


class TestClient(TestCase):

    def setUp(self):
        Base.metadata.create_all(ENGINE)

    def tearDown(self):
        Base.metadata.drop_all(ENGINE)

    def test_create_client(self):
        # GIVEN
        client = Client(name='Israel Silva')

        # WHEN
        with session_scope_factory() as session:
            session.add(client)
            session.commit()

        # THEN
        clients = session.query(Client).all()
        self.assertEqual(clients[0].id, 1)
        self.assertEqual(clients[0].name, 'Israel Silva')
