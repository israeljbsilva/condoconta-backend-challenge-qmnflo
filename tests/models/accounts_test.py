from decimal import Decimal
from unittest import TestCase

from models.clients import Client, Account, Base
from tests.factories import session_scope_factory, ENGINE


class TestAccount(TestCase):

    def setUp(self):
        Base.metadata.create_all(ENGINE)

    def tearDown(self):
        Base.metadata.drop_all(ENGINE)

    def test_create_account(self):
        # GIVEN
        client = Client(name='Israel Silva')

        # WHEN
        with session_scope_factory() as session:
            session.add(client)
            session.commit()
            account = Account(
                client_id=client.id
            )
            session.add(account)
            session.commit()

        # THEN
        accounts = session.query(Account).all()
        self.assertEqual(accounts[0].id, 1)
        self.assertEqual(accounts[0].client.name, 'Israel Silva')
        self.assertEqual(accounts[0].client_id, 1)
        self.assertEqual(accounts[0].balance, None)
        self.assertEqual(accounts[0].initial_balance, Decimal(100))
        self.assertEqual(accounts[0].client_id, 1)
