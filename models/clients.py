from sqlalchemy import Column, Integer, ForeignKey, DECIMAL, String
from sqlalchemy.orm import relationship

from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Account(Base):
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True)
    initial_balance = Column(DECIMAL, default=100.00)
    balance = Column(DECIMAL)
    client_id = Column(Integer, ForeignKey('client.id'))
    client = relationship('Client')


class Client(Base):
    __tablename__ = 'client'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
