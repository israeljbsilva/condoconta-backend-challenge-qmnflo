import ast
from contextlib import contextmanager

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from config import DEBUG, DATABASE_URL


ENGINE = create_engine(DATABASE_URL, echo=ast.literal_eval(DEBUG))


@contextmanager
def session_scope():
    Session = sessionmaker(bind=ENGINE)
    session = Session()
    session.expire_on_commit = False

    try:
        yield session
        session.commit()
    except:  # noqa
        session.rollback()
        raise
    finally:
        session.close()


def get_instance(session, model, **kwargs):
    with session.no_autoflush:
        instance = session.query(model).filter_by(**kwargs).all()
        return instance
