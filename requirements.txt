SQLAlchemy==1.4.*
psycopg2==2.9.*
pytest==6.2.*
flake8==3.9.*
pycodestyle==2.7.*
pytest-cov==2.12.*
